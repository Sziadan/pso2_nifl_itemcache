﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PSO2_TextHandler
{
    public partial class Form1 : Form
    {
        SegaTextParser textParser { get; set; }

        public Form1()
        {
            InitializeComponent();
            textParser = new SegaTextParser();
        }

        private void WriteLine(string msg)
        {
            tb_console.AppendText(msg + Environment.NewLine);
        }

        private void btn_open_Click(object sender, EventArgs e)
        {
            ofd.ShowDialog();
        }

        private void ofd_FileOk(object sender, CancelEventArgs e)
        {
            WriteLine(textParser.ParseFile(ofd.FileName));
        }
    }
}
