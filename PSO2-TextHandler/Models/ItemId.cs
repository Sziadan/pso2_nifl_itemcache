﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSO2_TextHandler.Models
{
    public class ItemId
    {
        public short Type { get; set; }
        public short Index { get; set; }
        public short SubA { get; set; }
        public short SubB { get; set; }

        public ItemId()
        {

        }
        
        public ItemId(byte[] b)
        {
            Type = BitConverter.ToInt16(b, 0);
            Index = BitConverter.ToInt16(b, 2);
            SubA = BitConverter.ToInt16(b, 4);
            SubB = BitConverter.ToInt16(b, 6);
        }

        public bool IsEmpty()
        {
            return  Type == 0 && 
                    Index == 0 && 
                    SubA == 0 && 
                    SubB == 0;
        }

        public override string ToString()
        {
            return $"{Type} {Index} {SubA} {SubB}";
        }
    }
}
