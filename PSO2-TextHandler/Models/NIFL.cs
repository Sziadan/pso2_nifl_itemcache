﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSO2_TextHandler.Models
{
    public class NIFL
    {
        public BlockHeader Header { get; set; }
        public int Flag { get; set; }
        public BlockPointer REL0 { get; set; }
        public BlockPointer NOF0 { get; set; }
        public int Padding { get; set; }

        public override string ToString()
        {
            return $@"Magic: {Header.Magic}
Size: {Header.Size}
Flag: {Flag}
REL0.Start: {REL0.Start}
REL0.Size: {REL0.Size}
NOF0.Start: {NOF0.Start}
NOF0.Size: {NOF0.Size}
Padding: {Padding}";
        }
    }

    public class BlockPointer
    {
        public int Start { get; set; }
        public int Size { get; set; }
    }
}
