﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSO2_TextHandler.Models
{
    public class SegaFile
    {
        public NIFL nifl { get; set; }
        public RELO rel0 { get; set; }

        public override string ToString()
        {
            return nifl.ToString() + Environment.NewLine + Environment.NewLine + rel0.ToString();
        }
    }
}
