﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSO2_TextHandler.Models
{
    public class BlockHeader
    {
        public string Magic { get; set; }
        public int Size { get; set; }

        public BlockHeader(byte[] b)
        {
            Magic = Encoding.ASCII.GetString(b, 0, 4);
            Size = BitConverter.ToInt32(b, 4);
        }
    }

    /// <summary>
    /// Real name REL0
    /// </summary>
    public class RELO
    {
        public BlockHeader Header { get; set; }
        public int PBlockSize { get; set; }
        public int Unknown { get; set; } // Possibly padding
        public int Unknown2 { get; set; } // Use pair ?
        public List<IPointerBlock> BlockEntries { get; set; }

        public RELO()
        {
            BlockEntries = new List<IPointerBlock>();
        }

        public override string ToString()
        {
            string output = $@"Magic: {Header.Magic}
Size: {Header.Size}
PBlockSize: {PBlockSize}
Unknown: {Unknown}
Unknown2: {Unknown2}";

            output += Environment.NewLine + $"REL0 - PointerBlock ({BlockEntries.Count})" + Environment.NewLine;
            return output;
        }
    }

    public interface IPointerBlock
    {

    }

    public class RegularPointerBlock : IPointerBlock
    {
        public int Offset { get; set; }
        public string Key { get; set; }
        public string Text { get; set; }

        public RegularPointerBlock()
        {

        }

        public RegularPointerBlock(byte[] b)
        {
            Offset = BitConverter.ToInt32(b, 0);
        }

        public void SetText(string s)
        {
            this.Key = this.Text;
            this.Text = s;
        }

        public override string ToString()
        {
            return this.Key + ": " + this.Text;
        }        
    }

    public class ItemPointerBlock : IPointerBlock
    {
        public ItemId Item_Id { get; set; }
        public int Offset { get; set; }
        public string item_name { get; set; }
        
        public ItemPointerBlock()
        {

        }

        public ItemPointerBlock(byte[] b)
        {
            Item_Id = new ItemId(b);
            Offset = BitConverter.ToInt32(b, 8);    
        }

        public override string ToString()
        {
            return $"{Item_Id, -12} | {item_name}";
        }
    }
}
