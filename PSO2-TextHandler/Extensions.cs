﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSO2_TextHandler
{
    public static class Extensions
    {
        public static string GetASCII(this byte[] b, int index, int count)
        {
            int real_count = count;
            for (int i = 0; i < b.Length; i++)
            {
                if (b.Length > index + i)
                {
                    if (b[index + i] == 0)
                    {
                        real_count = i;
                        break;
                    }
                }
            }

            return Encoding.ASCII.GetString(b, index, real_count);
        }

        public static string GetUnicode(this byte[] b, int index, int count)
        {
            int real_count = count;
            for(int i = 0; i < b.Length; i+=2)
            {
                if (b.Length > index + i + 1)
                {
                    if (b[index + i] == 0 && b[index + i + 1] == 0)
                    {
                        real_count = i;
                        break;
                    }
                }
            }

            return Encoding.GetEncoding("UTF-16").GetString(b, index, real_count);
        }
    }
}
