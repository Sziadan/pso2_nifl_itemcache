﻿using PSO2_TextHandler.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSO2_TextHandler
{
    public class SegaTextParser
    {
        public string FileText { get; set; }

        private byte[] bytes { get; set; }
        private int pos { get; set; }
        private bool ItemIdFile { get; set; }
        private bool Pair { get; set; }

        public SegaTextParser()
        {
            FileText = string.Empty;
            ItemIdFile = false;
        }

        #region Reader Stuff
        private byte[] Read(int count)
        {
            if (pos + count >= this.bytes.Length)
                return new byte[0];

            byte[] b = new byte[count];
            Buffer.BlockCopy(this.bytes, pos, b, 0, count);
            pos += count;
            return b;
        }

        private byte[] Peak(int count)
        {
            if (pos + count >= this.bytes.Length)
                return new byte[0];

            byte[] b = new byte[count];
            Buffer.BlockCopy(this.bytes, pos, b, 0, count);
            return b;
        }

        private void SetCursor(int pos)
        {
            this.pos = pos;
        }

        private BlockHeader GetHeader(int start_pos = -1)
        {
            if (start_pos >= 0)
                SetCursor(start_pos);

            return new BlockHeader(Read(8));
        }
        #endregion

        public string ParseFile(string file)
        {
            pos = 0;
            if (!File.Exists(file))
                return "File not found.";
            bytes = File.ReadAllBytes(file);
            ItemIdFile = file.EndsWith(".inca");

            var sega_file = new SegaFile();

            sega_file.nifl = GetNIFL();
            sega_file.rel0 = GetREL0(sega_file.nifl.REL0.Start);

            StringBuilder sb = new StringBuilder();
            foreach(var item in sega_file.rel0.BlockEntries)
            {
                sb.AppendLine(item.ToString());
            }
            File.WriteAllText("Parsed.txt", sb.ToString());

            return sega_file.ToString();
        }

        private NIFL GetNIFL()
        {
            var header = GetHeader();

            var b = Read(header.Size);
            if (b.Length == 0)
                return new NIFL();

            return new NIFL()
            {
                Header = header,
                Flag = BitConverter.ToInt32(b, 0),
                REL0 = new BlockPointer()
                {
                    Start = BitConverter.ToInt32(b, 4),
                    Size = BitConverter.ToInt32(b, 8)
                },
                NOF0 = new BlockPointer()
                {
                    Start = BitConverter.ToInt32(b, 12),
                    Size = BitConverter.ToInt32(b, 16)
                }
            };
        }
        
        private RELO GetREL0(int offset)
        {
            var header = GetHeader(offset);

            var b = Read(header.Size);
            if (b.Length == 0)
                return new RELO();

            int cursor = 0;
            var rel0 = new RELO()
            {
                Header = header,
                PBlockSize = BitConverter.ToInt32(b, cursor),
                Unknown = BitConverter.ToInt32(b, cursor += 4),
                Unknown2 = BitConverter.ToInt32(b, cursor += 4)
            };
            cursor += 4;

            if (ItemIdFile)
            {
                for (; cursor < rel0.PBlockSize; cursor += 12)
                {
                    var entry = new ItemPointerBlock(
                            b.Skip(cursor)
                            .Take(12)
                            .ToArray());
                    entry.item_name = b.GetUnicode(entry.Offset - 8, 50).Trim('\0');
                    rel0.BlockEntries.Add(entry);
                    if (entry.Item_Id.IsEmpty())
                        break;
                }
            }
            else
            {
                Pair = true;
                int c = 0;
                for (; cursor < rel0.PBlockSize; cursor += 4)
                {
                    var entry = new RegularPointerBlock(
                            b.Skip(cursor)
                            .Take(4)
                            .ToArray());
                    if (entry.Offset - 8 < 0)
                        break;

                    if (Pair && c % 2 == 1)
                    {
                        ((RegularPointerBlock)rel0.BlockEntries[rel0.BlockEntries.Count - 1]).SetText(b.GetUnicode(entry.Offset - 8, 150).Trim('\0'));
                    }
                    else
                    {
                        entry.Text = b.GetASCII(entry.Offset - 8, 50).Trim('\0');
                        if (string.IsNullOrEmpty(entry.Text))
                            break;
                        rel0.BlockEntries.Add(entry);
                    }
                    c++;
                }
            }

            return rel0; 
        }
    }
}
